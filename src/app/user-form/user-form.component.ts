import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user: User = {};

  title: string = 'Users'

  subtitle: string = '';

  constructor(private userService: UserService, 
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    const id: number = +this.route.snapshot.paramMap.get('id');

    console.log(id)
    if (id) {
      this.userService.getUser(id).subscribe(_user => {
        this.user = _user;  
        console.log(this.user)
      })

      this.subtitle = 'Update'
    } else {
      this.subtitle = 'Creation'
    }
  }

  store(user: User): void {

    if (user.id) {
      this.userService.update(user).subscribe(response => {
        console.log(response)
        

        Swal.fire({
          type: 'success',
          title: 'Update User',
          text: `User ${user.firstname} ${user.lastname} has been update`,
          
        });

        this.router.navigate(['users'])
        
      })
    } else {
      this.userService.create(user).subscribe(response => {
        console.log(response)
        

        Swal.fire({
          type: 'success',
          title: 'Create User',
          text: `User ${user.firstname} ${user.lastname} has been created`
        })

        this.router.navigate(['users'])
      })
    }
  }
}
