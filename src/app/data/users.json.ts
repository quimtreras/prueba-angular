export const USERS: User[] = [
  {
    "id": 1,
    "username": "jdoe",
    "firstname": "John",
    "lastname": "Doe",
    "password": "jdoepass",
    "email": "jdoe@mail.com",
    "age": 34,
    "active": true
  },
  {
    "id": 2,
    "username": "mtomas",
    "firstname": "Markus",
    "lastname": "Tomas",
    "password": "markuspass",
    "email": "mtomas@mail.com",
    "age": 70,
    "active": true
  }
];