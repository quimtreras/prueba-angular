import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlBase: string = 'http://localhost:8080/users';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient, private router: Router) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get(`${this.urlBase}`).pipe(map(response => {
      return response as User[]
    }))
  }

  getPaginatedUsers(currentPage: number, numberOfRecordPerPage: number): Observable<any> {
    return this.http.get(`${this.urlBase}/paginated?page=${currentPage - 1}&numberOfRecords=${numberOfRecordPerPage}`).pipe(
      map(response => {
        return response
      })
    )
  }

  getUser(id: number): Observable<User> {
    return this.http.get(`${this.urlBase}/${id}`).pipe(
      catchError(err => {
        console.log(err)
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: `User with id ${id} does not exist`
        })
        this.router.navigate(['users']);
        return throwError(err)
      }),
      map(response => {
      return response as User;
    }))
  }

  create(user: User): Observable<User> {

    console.log('create')

    return this.http.post<User>(`${this.urlBase}`, user, this.httpOptions).pipe(
      catchError(err => {
        Swal.fire({
          type: 'error',
          title: 'Error creating user',
          text: `User has not been able to be created`,          
        });

        return throwError(err);
      }),
      tap(response => console.log(response)))
  }

  update(user: User): Observable<User> {    
    return this.http.put(`${this.urlBase}/${user.id}`, user, this.httpOptions).pipe(
      catchError(err => {
        Swal.fire({
          type: 'error',
          title: 'Error creating user',
          text: `User  has not been able to be updated`          
        });
        return throwError(err)
      })
    )
  }

  delete(user: User): Observable<any> {
    return this.http.delete(`${this.urlBase}/${user.id}`).pipe(
      catchError(err => {
        Swal.fire({
          type: 'error',
          title: 'Error deleting user',
          text: `User ${user.firstname} ${user.lastname} has not be able to be deleted`,          
        });
        return throwError(err);
      }),
    )
  }
}
