import { Component, OnInit, OnChanges } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { swalWithBootstrapButtons } from '../utils/swalUtils';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
 
  private title: string = "Users";

  private cardTitle: string = "Operations";

  private userList: User[];

  paginator: any;

  currentPage: number = 1;

  numberOfRecordPerPage: number = 10;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.paginate();
  }  

  updateUser(user: User) {
    this.router.navigate(['users/form', user.id] )
  }

  deleteUser(user: User) {
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.userService.delete(user).subscribe(response => {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            `User ${user.firstname} ${user.lastname} has been deleted.`,
            'success'
          )

          if (this.currentPage > 1 && this.paginator.recordsInCurrentPage <= 1) {
            this.currentPage--;
          }
  
          this.paginate()
        })        


      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          `User ${user.firstname} ${user.lastname} is safe :)`,
          'error'
        )
      }
    })
  }

  updateList(user: User) {
    this.userList = this.userList.filter( _user => _user.id != user.id)
  }

  setCurrentPage(current: number):void {

    this.currentPage = current;

    this.paginate();
  }

  paginate(): void {
    this.userService.getPaginatedUsers(this.currentPage, this.numberOfRecordPerPage).subscribe(response => {
      this.userList = response.data as User[];
      this.paginator = response;
    })
  }

  
}
