import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})


export class PaginatorComponent implements OnInit, OnChanges {
  

  @Input()
  paginator: any;

  private pages: number[]; 

  @Output()
  currentPageSelected = new EventEmitter<number>();

  private currentPage: number;

  private numberOfPages: number
  constructor() { }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {

    this.numberOfPages = this.paginator.pages;
    
    this.currentPage = this.paginator.currentPage + 1;

    let pages: number[] = [];

    for (let i = 0; i < this.numberOfPages; i++) {
      pages[i] = i + 1;
    }


    this.pages = pages;
  }

  ngOnInit() {
/*
    let numberOfPages: number = this.paginator.pages;

    this.currentPage = this.paginator.currentPage + 1;

    for (let i = 0; i < numberOfPages; i++) {
      this.pages[i] = i + 1;
    }*/
  }

  updateCurrentPage(newCurrentPage: number): void {    
    this.currentPage = newCurrentPage

    this.currentPageSelected.emit(this.currentPage);
  }

  nexPage(): void {
    if (this.currentPage + 1 > this.pages.length) {
      this.currentPage = 1;
    } else {
      this.currentPage++;
    }
    this.currentPageSelected.emit(this.currentPage);
  }

  previousPage(): void {
   
    if (this.currentPage - 1 === 0) {
      this.currentPage = this.pages.length;
    } else {
      this.currentPage--;
    }
    this.currentPageSelected.emit(this.currentPage);
  }


}
